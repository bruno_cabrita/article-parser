import { createRouter, createWebHistory, RouteRecordRaw } from "vue-router";
import Home from "../views/Home.vue";
import Article from "../views/Article.vue";

const routes: Array<RouteRecordRaw> = [
  {
    path: "/:catchAll(.*)",
    redirect: { name: "Home" }
  },
  {
    path: "/",
    name: "Home",
    component: Home
  },
  {
    path: "/article/:url",
    name: "Article",
    component: Article
  }
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
});

export default router;
