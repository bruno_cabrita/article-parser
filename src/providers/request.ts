import axios from "axios";

function stripProtocolFromUrl(url: URL): string {
  const protocol = url.protocol;
  const urlStr = url.href;
  return urlStr.replace(protocol+'//', '');
}

function isValidHttpUrl(urlString: string): boolean {
  let url;
  try {
    url = new URL(urlString);
  } catch (err) {
    return false;  
  }
  return url.protocol === "http:" || url.protocol === "https:";
}

function fetchArticle(url: URL) {
  return new Promise((resolve, reject) => {
    const corsUrl = "https://cors-anywhere.metacriacoes.com/" + stripProtocolFromUrl(url);
    axios({
      method: 'get',
      url: corsUrl,
      data: {},
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        'Content-Type': 'text/plain',
      }
    })
    .then(res => res.data)
    .then(res => {
      resolve(res);
    })
    .catch(err => {
      reject(err);
    });
  });
}

export { fetchArticle, isValidHttpUrl };