import { Source, convertAttribute } from "./mod";

const sourceConfig: Source = {
  name: "Público",
  articleSelector: ".story__content",
  junkSelectors: new Set([
    ".story__labels",
    ".story__meta",
    "aside",
    "script",
    "footer",
    "#extraContent",
    ".media-badge",
    ".credit",
    ".kicker--exclusive",
    "figcaption",
    "ul",
    "hr"
  ]),
  modifiers: new Map([
    [
      "img[data-src]",
      (elem: Element) => {
        convertAttribute(elem, "data-src", "src");
      }
    ]
  ]),
  classes: new Map([
    ["h1", "text-3xl font-bold mb-4"],
    [".lead", "font-bold mb-4"],
    ["img", "mb-4"],
    ["p", "mb-4"],
    ["a", "text-blue-600 underline"],
  ]),
  // styles: new Map([
  //   [".selector", "rule: value"],
  // ]),
  safeAttribures: new Set(["href", "target", "src"])
};

export default new Map([["www.publico.pt", sourceConfig]]);
