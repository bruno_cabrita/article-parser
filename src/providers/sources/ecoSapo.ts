import { Source } from "./mod";

const sourceConfig: Source = {
  name: "Eco Sapo",
  articleSelector: "article",
  junkSelectors: new Set([
    "aside",
    "script",
    ".tags",
    ".meta",
    ".social__follow",
    "footer",
    ".infobox",
    ".resize-sensor",
    ".sapopub",
    ".lightbox",
    ".button"
  ]),
  classes: new Map([
    ["h1", "text-3xl font-bold mb-4"],
    ["img", "mb-4"],
    ["p", "mb-4"],
    ["a", "text-blue-600 underline"],
    ["h2.entry__lead", "font-bold mb-4"]
  ]),
  safeAttribures: new Set(["href", "target", "src"])
};

export default new Map([["eco.sapo.pt", sourceConfig]]);