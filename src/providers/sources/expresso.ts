import { Source } from "./mod";

const sourceConfig: Source = {
  name: "Expresso",
  articleSelector: ".article-2020-wrapper",
  junkSelectors: new Set([
    ".exclusive-blocker",
    ".shareTools2020",
    ".mainSection",
    ".aside",
    ".credits",
    "script",
  ]),
  classes: new Map([
    ["h1", "text-3xl font-bold mb-4"],
    [".lead", "font-bold mb-4"],
    ["img", "mb-4"],
    ["p", "mb-4"],
    ["a", "text-blue-600 underline"],
  ]),
  // styles: new Map([
  //   [".selector", "rule: value"],
  // ]),
  safeAttribures: new Set([
    "href",
    "target",
    "src",
  ]),
};

export default new Map([["expresso.pt", sourceConfig]]);