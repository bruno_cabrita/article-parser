import publico from "./publico";
import expresso from "./expresso";
import obervador from "./obervador";
import jn from "./jn";
import ecoSapo from "./ecoSapo";

const allSources = new Map([
  ...publico,
  ...expresso,
  ...obervador,
  ...jn,
  ...ecoSapo,
]);

interface Source {
  name: string;
  articleSelector: string;
  junkSelectors?: Set<string>;
  classes?: Map<string, string>;
  styles?: Map<string, string>;
  modifiers?: Map<string, Function>;
  safeAttribures?: Set<string>;
}

const protectedAttibutes: Set<string> = new Set([
  "protected-class",
  "protected-style"
]);

function HTMLNodeIterator(task: Function, node: Element | ChildNode) {
  for(let x = 0; x < node.childNodes.length; x++) {
    const childNode = node.childNodes[x];
    task(childNode);
    if(childNode.childNodes.length > 0)
    HTMLNodeIterator(task, childNode);
  }
}

function removeAttributes(safeAttributes: Set<string>, elem: Element) {
  if(elem.attributes) {
    for(let i = elem.attributes.length - 1 ; i > -1; i--) {
      const attr = elem.attributes[i];
      if(!safeAttributes.has(attr.name)) {
        elem.removeAttribute(attr.name);
      }
    }
  }
}

function convertAttribute(elem: Element, fromAttr: string, toAttr: string) {
  const attrValue = elem.getAttribute(fromAttr) as string;
  elem.removeAttribute(fromAttr);
  elem.setAttribute(toAttr, attrValue);
}
  
function getParsedArticle(url: URL, articleHtml: string): string {
  const source = allSources.get(url.hostname)!;
  const doc = new DOMParser().parseFromString(articleHtml, "text/html");
  const articleElem = doc.querySelector(source.articleSelector)!;

  // removes junk elements
  if(source.junkSelectors) {
    source.junkSelectors.forEach(selector => {
      const nodeList = articleElem.querySelectorAll(selector);
      nodeList.forEach(nodeToRemove => {
        nodeToRemove.remove();
      });
    });
  }

  // run modifiers
  if(source.modifiers) {
    source.modifiers.forEach((task, selector) => {
      const nodeList = articleElem.querySelectorAll(selector);
      nodeList.forEach(node => {
        task(node);
      });
    });
  }

  // add protected-class attribute if necessary
  if(source.classes) {
    source.classes.forEach((value, selector) => {
      const nodeList = articleElem.querySelectorAll(selector);
      nodeList.forEach(nodeToRemove => {
        nodeToRemove.setAttribute("protected-class", value);
      });
    });
  }

  // apply protected-style attribute if necessary
  if(source.styles) {
    source.styles.forEach((value, selector) => {
      const nodeList = articleElem.querySelectorAll(selector);
      nodeList.forEach(nodeToRemove => {
        nodeToRemove.setAttribute("protected-style", value);
      });
    });
  }

  // removes unused attributes
  if(source.safeAttribures) {
    const safeAttribures: Set<string> = new Set([...protectedAttibutes, ...source.safeAttribures])
    HTMLNodeIterator((elem: Element) => {
      removeAttributes(safeAttribures, elem);
    }, articleElem);
  }

  // convert protected-class to class
  if(source.classes) {
    const nodeList = articleElem.querySelectorAll("[protected-class]");
    nodeList.forEach(node => {
      convertAttribute(node, "protected-class", "class");
    });
  }

  // convert protected-style to style
  if(source.styles) {
    const nodeList = articleElem.querySelectorAll("[protected-style]");
    nodeList.forEach(node => {
      convertAttribute(node, "protected-style", "style");
    });
  }

  const sourceLabelHtml = `<div class="uppercase mb-4 text-sm">${source.name}</div>`;

  // console.log(articleElem.innerHTML);
  return sourceLabelHtml + articleElem.innerHTML;
}

function hasUrlSource(url: URL): boolean {
  return allSources.has(url.hostname);
}

function useSources() {
  return {
    hasUrlSource,
    getParsedArticle,
  }
};

export { useSources, Source, convertAttribute }