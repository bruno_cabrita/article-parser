import { Source, convertAttribute } from "./mod";

const sourceConfig: Source = {
  name: "Observador",
  articleSelector: "article",
  junkSelectors: new Set([
    "aside",
    "script",
    ".article-head-follow",
    ".article-head-authors",
    ".article-head-meta",
    "details",
    "figcaption",
    ".piano-article-blocker",
    "#share-piano",
    ".article-footer",
    ".obs-ad-container",
    "iframe",
    "blockquote",
    ".left-column",
    ".right-column",
    "[class*='recirculation']",
    "[id*='recirculation']",
    ".show-if-premium"
  ]),
  modifiers: new Map([
    [
      "img[data-src]",
      (elem: Element) => {
        convertAttribute(elem, "data-src", "src");
      }
    ]
  ]),
  classes: new Map([
    ["h1", "text-3xl font-bold mb-4"],
    ["img", "mb-4"],
    ["p", "mb-4"],
    ["a", "text-blue-600 underline"],
    [".article-head-content-headline-lead", "font-bold mb-4"]
  ]),
  safeAttribures: new Set(["href", "target", "src"])
};

export default new Map([["observador.pt", sourceConfig]]);
