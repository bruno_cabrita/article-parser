import { Source } from "./mod";

const sourceConfig: Source = {
  name: "JN",
  articleSelector: "article.mainEntity",
  junkSelectors: new Set([
    "aside.t-article-funcs-1",
    "aside.t-article-funcs-2",
    "script",
    "figcaption",
    ".t-article-footer",
    ".text-pub",
    'h3[rel="articleSection"]',
    '.t-article-sticky-bar-1',
  ]),
  classes: new Map([
    ["h1", "text-3xl font-bold mb-4"],
    ["img", "mb-4"],
    ["p", "mb-4"],
    ["a", "text-blue-600 underline"],
  ]),
  safeAttribures: new Set([
    "href",
    "target",
    "src",
  ]),
};

export default new Map([["www.jn.pt", sourceConfig]]);